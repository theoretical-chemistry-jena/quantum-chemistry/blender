# Creates: cl_field.ind_Ffe.png, qm_field.ind_Ffe.png, tot_field.ind_Ffe.png
# -*- coding: utf-8 -*-
from gpaw.mpi import world
#assert world.size == 1, 'This script should be run in serial mode (with one process).'

import numpy as np
import matplotlib.pyplot as plt

from gpaw.inducedfield.inducedfield_base import BaseInducedField
from gpaw.tddft.units import aufrequency_to_eV
from gpaw.tddft import TDDFT
from ase.units import Bohr
import sys
import argparse

# Helper function
def do_plot(d_g, ng, box, atoms, cut_axis, cut_height):
    # Take slice of data array
    if cut_axis == 0:
        d_yx = d_g[cut_height, :, :]
        y = np.linspace(0, box[2], ng[2] + 1)[:-1]
        dy = box[2] / (ng[2] + 1)
        y += dy * 0.5
        ylabel = u'z [Å]'
        x = np.linspace(0, box[1], ng[1] + 1)[:-1]
        dx = box[1] / (ng[1] + 1)
        x += dx * 0.5
        xlabel = u'y [Å]'
    elif cut_axis == 1:
        d_yx = d_g[:, cut_height, :]
        y = np.linspace(0, box[0], ng[0] + 1)[:-1]
        dy = box[0] / (ng[0] + 1)
        y += dy * 0.5
        ylabel = u'x [Å]'
        x = np.linspace(0, box[2], ng[2] + 1)[:-1]
        dx = box[2] / (ng[2] + 1)
        x += dx * 0.5
        xlabel = u'z [Å]'
    elif cut_axis == 2:
        d_yx = d_g[:, :, cut_height]
        y = np.linspace(0, box[0], ng[0] + 1)[:-1]
        dy = box[0] / (ng[0] + 1)
        y += dy * 0.5
        ylabel = u'x [Å]'
        x = np.linspace(0, box[1], ng[1] + 1)[:-1]
        dx = box[1] / (ng[1] + 1)
        x += dx * 0.5
        xlabel = u'y [Å]'

    # Plot
    plt.figure()
    ax = plt.subplot(1, 1, 1)
    X, Y = np.meshgrid(x, y)
    #plt.contourf(X, Y, d_yx, 20, cmap=plt.cm.jet)
    if cut_axis == 0:
        plt.imshow(d_yx, cmap=plt.cm.jet, interpolation=None, extent=(0, box[1], 0, box[2]))
    elif cut_axis == 1:
        plt.imshow(d_yx, cmap=plt.cm.jet, interpolation=None, extent=(0, box[2], 0, box[0]))
    elif cut_axis == 2:
        plt.imshow(d_yx, cmap=plt.cm.jet, interpolation=None, extent=(0, box[1], 0, box[0]))
    plt.colorbar()
    for atom in atoms:
        pos = atom.position
        if cut_axis == 0:
            plt.scatter(pos[1], pos[2], s=10, c='b', marker='o')
        elif cut_axis == 2:
            plt.scatter(pos[2], pos[0], s=10, c='b', marker='o')
        elif cut_axis == 2:
            plt.scatter(pos[1], pos[0], s=10, c='b', marker='o')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim([x[0], x[-1]])
    plt.ylim([y[0], y[-1]])
    ax.set_aspect('equal')

class Atom:

    def __init__(self, atomic_number, location, id_num):
        self.at_num = atomic_number
        self.location = location # np.array
        self.id_num = id_num

class Cube:

    def __init__(self, offset=None, ng=None, basis=None, data=None, atoms=[]):
        
        self.offset = offset
        self.ng = ng
        self.basis = basis
        self.data = data
        self.atoms = []

    def load_ind(self, ind, w=0):
        '''Load cube file data from BaseInducedField object of GPAW'''

        #freq = ind.omega_w[w] * aufrequency_to_eV  # Frequency
        box = np.diag(ind.atoms.get_cell())        # Calculation box
        self.data = ind.Ffe_wg[w]                        # Data array
        self.ng = self.data.shape                             # Size of grid
        self.offset = np.array( [0., 0., 0.] )
        # calculate basis
        self.basis = [ np.array( [ box[0] / (self.ng[0] - 1), 0., 0. ] ),
                       np.array( [ 0., box[1] / (self.ng[1] - 1), 0. ] ),
                       np.array( [ 0., 0., box[2] / (self.ng[2] - 1) ] ) ]
        
        # get atoms information
        num = ind.atoms.get_atomic_numbers()
        pos = ind.atoms.get_positions()
        for i in range(len(num)):
            self.atoms.append( Atom(num[i], pos[i], i) )

        # print information
        if True:
            print("Grid offset:", self.offset)
            print("Grid size:", self.ng)
            print("Basis vectors for grid are:")
            for xyz in range(3):
                print(self.basis[xyz])
            print("Got %i atoms" % len(self.atoms))
        

    def load_tddft(self, td_calc):
        '''Load cube file data from GPAW restart file of TDDFT calculation'''

        poisson_solver = td_calc.hamiltonian.poisson
        box = np.diagonal(poisson_solver.cl.gd.cell_cv) * Bohr # in Ang
        self.data = poisson_solver.classical_material.beta[0]
        self.ng = self.data.shape

        self.offset = np.array( [0., 0., 0.] )
        # calculate basis
        self.basis = [ np.array( [ box[0] / (self.ng[0] - 1), 0., 0. ] ),
                       np.array( [ 0., box[1] / (self.ng[1] - 1), 0. ] ),
                       np.array( [ 0., 0., box[2] / (self.ng[2] - 1) ] ) ]
        
        # get atoms information
        atoms = td_calc.atoms
        pos = atoms.positions + poisson_solver.qm.corner1 * Bohr # in Ang
        num = atoms.get_atomic_numbers()

        for i in range(len(num)):
            self.atoms.append( Atom(num[i], pos[i], i) )
        
        # print information
        if True:
            print("Grid offset:", self.offset)
            print("Grid size:", self.ng)
            print("Basis vectors for grid are:")
            for xyz in range(3):
                print(self.basis[xyz])
            print("Got %i atoms" % len(self.atoms))
 

    def save(self, fname):
        print("Saving as %s" % fname)
        outstr = "Cube file from GPAW BaseInducedField\nbla\n"
        outstr += "%i %f %f %f\n" % (len(self.atoms), self.offset[0], self.offset[1], self.offset[2])
        for xyz in range(3):
            outstr += "%i %f %f %f\n" % (self.ng[xyz], self.basis[xyz][0], self.basis[xyz][1], self.basis[xyz][2])
        for atom in self.atoms:
            outstr += "%i %f %f %f %f\n" % (atom.at_num, 0., atom.location[0], atom.location[1], atom.location[2])
        
        np.set_printoptions(threshold=np.inf)
        data_str = ''
        for i, sub_array in enumerate(self.data):
            print("Progress: %i of %i" % (i, len(self.data)))
            data_flat = sub_array.flatten()
            data_str += str(data_flat).replace('[', '').replace(']','').replace(',', ' ')
            data_str += '\n'
        #data_str = np.array_str(data_flat, max_line_width=80).replace('[', ' ').replace(']', ' ')

        outstr += data_str

        with open(fname, 'w') as f:
            f.write(outstr)





if __name__ == '__main__':

    
    parser = argparse.ArgumentParser(description='Script to convert GPAW results to cube files for plotting with Blender.')
    parser.add_argument('-f', '--file', action='store', help='Input file. GPAW restart file or induced field.')
    parser.add_argument('-d', '--data', action='store', default='geom', help='Data to store: "geom" (default) for storing classical material info; "field" for storing field information')
    parser.add_argument('-o', '--output', action='store', default='output.cube', help='Output file.')
    parser.add_argument('-w', '--w', action='store', default=0, help='Frequency index of induced field. Zero by default.')

    args = parser.parse_args()
    print(args.data)
    if args.data == 'geom':
        td_calc = TDDFT(args.file)
        if world.rank == 0:
            cub = Cube()
            cub.load_tddft(td_calc)
            cub.save(args.output)
    else:
        ind = BaseInducedField(args.file, readmode='all')
        if world.rank == 0:
            cub = Cube()
            cub.load_ind(ind, int(args.w))
            cub.save(args.output)

