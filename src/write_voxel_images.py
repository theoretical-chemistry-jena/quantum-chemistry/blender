"""Scripts for loading molecular geometries into Blender.

    Main use is to define the "molecule" object, which can be used to
    draw models of molecular coordinates.

    Written by Scott Hartley, www.hartleygroup.org and
    blog.hartleygroup.org.

    Hosted here:

    https://github.com/scotthartley/blmol

    Some aspects (especially rotating the bond cylinders) based on this
    wonderful blog post from Patrick Fuller:
    
    http://patrick-fuller.com/molecules-from-smiles-molfiles-in-blender/

    His project is hosted here:

    https://github.com/patrickfuller/blender-chemicals
"""

import os
import numpy as np, time
#import mcubes
#from scipy.misc import toimage
from PIL import Image as pimg
from scipy.spatial import distance_matrix
from math import isclose
import math
#from mathutils import Matrix, Vector

# Python outside of Blender doesn't play all that well with bpy, so need
# to handle ImportError.
try:
    import bpy
    import bmesh
    bpy_avail = True
except ImportError:
    bpy_avail = False

# Dictionary of color definitions (RGB tuples). Blender RGB colors can
# be conveniently determined by using a uniform png of the desired color
# as a background image, then using the eyedropper. Colors with the
# "_cb" tag are based on colorblind-safe colors as described in this
# Nature Methods editorial DOI:10.1038/nmeth.1618.
COLORS = {
    'black': (0, 0, 0, 1),
    'dark_red': (0.55, 0, 0, 1),
    'gray': (0.2, 0.2, 0.2, 1),
    'dark_gray': (0.1, 0.1, 0.1, 1),
    'green': (0.133, 0.545, 0.133, 1),
    'green_surface': (0.133, 0.545, 0.133, 0.3),
    'red_surface': (0.8, 0., 0., 0.3),
    'blue_surface': (0., 0., 0.8, 0.3),
    'dark_green': (0.1, 0.5, 0.1, 1),
    'indigo': (0.294, 0, 0.509, 1),
    'light_gray': (0.7,0.7,0.7, 1),
    'orange': (1.0, 0.647, 0, 1),
    'purple': (0.627, 0.125, 0.941, 1),
    'red': (0.8, 0, 0, 1),
    'royal_blue': (0.255, 0.412, 0.882, 1),
    'white': (1.0, 1.0, 1.0, 1),
    'yellow': (1.0, 1.0, 0, 1),
    'blue_cb': (0, 0.168, 0.445, 1),
    'bluish_green_cb': (0, 0.620, 0.451, 1),
    'orange_cb': (0.791, 0.347, 0, 1),
    'reddish_purple_cb': (0.800, 0.475, 0.655, 1),
    'sky_blue_cb': (0.337, 0.706, 0.914, 1),
    'vermillion_cb': (0.665, 0.112, 0, 1),
    'yellow_cb': (0.871, 0.776, 0.054, 1)
    }

ATOMIC_NUMBERS = {
    'H': 1,
    'B': 5,
    'C': 6,
    'N': 7,
    'O': 8,
    'F': 9,
    'MG': 12,
    'P': 15,
    'S': 16,
    'CL': 17,
    'ZN': 30,
    'BR': 35,
    'I': 53
    }

# Dictionary of Van der Waals radii, by atomic number, from Wolfram
# Alpha.
RADII = { 
    1: 1.20,
    5: 1.92, # From wikipedia
    6: 1.70,
    7: 1.55,
    8: 1.52,
    9: 1.47,
    12: 1.73, # From wikipedia
    15: 1.80,
    16: 1.80,
    17: 1.75,
    30: 1.39, # From wikipedia
    35: 1.85,
    53: 1.98
    }

# Dictionaries of colors for drawing elements, by atomic number. Used by
# several functions when the `color = 'by_element'` option is passed.
ELEMENT_COLORS = { 
    1: 'white',
    5: 'orange',
    6: 'gray',
    7: 'royal_blue',
    8: 'red',
    9: 'green',
    12: 'dark_green',
    15: 'orange',
    16: 'yellow',
    17: 'green',
    30: 'dark_gray',
    35: 'dark_red',
    53: 'indigo'
    }

# Conversion factors for 1 BU. Default is typically 1 nm. Assumes
# geometries are input with coords in angstroms.
UNIT_CONV = { 
    'nm': 0.1,
    'A': 1.0 
    }


def _create_new_volume_material(name, color1, color2, cube_data, fname, frame):
    """Create a new material.

    Args:
        name (str): Name for the new material (e.g., 'red')
        color (tuple): RGB color for the new material (diffuse_color) 
            (e.g., (1, 0, 0))
    Returns:
        The new material.
    """
    
    offset = cube_data[0]
    ng = cube_data[1]
    basis = cube_data[2]
    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    nodes = mat.node_tree.nodes
    nodes.clear()
    vol = nodes.new(type="ShaderNodeScript")
    vol.script = bpy.data.texts["cube_volume_shader.osl"]
    vol.inputs["Color1"].default_value = color1
    vol.inputs["Color2"].default_value = color2
    vol.inputs["sizex"].default_value = ng[0]
    vol.inputs["sizey"].default_value = ng[1]
    vol.inputs["sizez"].default_value = ng[2]
    vol.inputs["xoffset"].default_value = offset[0]
    vol.inputs["yoffset"].default_value = offset[1]
    vol.inputs["zoffset"].default_value = offset[2]
    vol.inputs["dx"].default_value = basis[0][0]
    vol.inputs["dy"].default_value = basis[1][1]
    vol.inputs["dz"].default_value = basis[2][2]
    vol.inputs["FileNameBase"].default_value = fname
    vol.inputs["frame"].default_value = frame
    node_output = nodes.new(type="ShaderNodeOutputMaterial")
    node_output.location = 400, 0

    links = mat.node_tree.links
    link = links.new(vol.outputs[0], node_output.inputs[1])
    return mat

def _create_new_material(name, color, alpha=1.):
    """Create a new material.

    Args:
        name (str): Name for the new material (e.g., 'red')
        color (tuple): RGB color for the new material (diffuse_color) 
            (e.g., (1, 0, 0))
    Returns:
        The new material.
    """
    
    mat = bpy.data.materials.new(name)
    mat.use_nodes = True
    nodes = mat.node_tree.nodes
    nodes.clear()
    bsdf = nodes.new(type="ShaderNodeBsdfPrincipled")
    bsdf.inputs[0].default_value = color
    bsdf.inputs[18].default_value = alpha

    node_output = nodes.new(type="ShaderNodeOutputMaterial")
    node_output.location = 400, 0

    links = mat.node_tree.links
    link = links.new(bsdf.outputs[0], node_output.inputs[0])
    return mat

    ### old version
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = color
    #mat.diffuse_shader = 'OREN_NAYAR'
    #mat.diffuse_intensity = 0.8
    mat.roughness = 0.5
    mat.specular_color = (1, 1, 1)
    #mat.specular_shader = 'BLINN'
    mat.specular_intensity = 0.2
    #mat.specular_hardness = 25
    #mat.ambient = 1
    #mat.use_transparent_shadows = True
    # mat.subsurface_scattering.use = True

    return mat

def draw_box(name, cube_data, fname, frame, units='nm'):
    print("Drawing box")
    offset = cube_data[0]
    ng = cube_data[1]
    basis = cube_data[2]
    r = np.linalg.norm( ng[0] * basis[0] ) * UNIT_CONV[units]
    
    # offset from octopus grid box relativ to cube located at origin in blender
    loc = offset*UNIT_CONV[units] + r / 2.

    # create cube as bmesh
    bm = bmesh.new()
    bmesh.ops.create_cube(bm, size=r)
    # convert to mesh
    me = bpy.data.meshes.new(name)
    bm.to_mesh(me)
    bm.free()

    # Assign mesh to object and place in space.
    box_cube = bpy.data.objects.new("cubefile_box", me)
    bpy.context.collection.objects.link(box_cube)
    box_cube.location = tuple(loc)
    
    # set material
    mat = "%s_VolumeMaterial" % name
    #if not mat in bpy.data.materials:
    _create_new_volume_material(mat, (1,0,0,1), (0,0,1,1), cube_data, fname, frame)
    
    box_cube.data.materials.append(bpy.data.materials[mat])
    bpy.context.object.name = name
    return

def draw_orbital(name, cube_data, thresh=0.02, alpha=0.3, units='nm'):
    """Draw two surfaces with positive and negative threshold each and combine them in one object."""
    
    offset = cube_data[0]
    ng = cube_data[1]
    basis = cube_data[2]
    data = cube_data[3]
    b = np.array(basis)
    vertices, triangles = mcubes.marching_cubes(data, thresh)
    vertices = vertices.tolist()
    triangles = triangles.tolist()
    for i in range(len(vertices)):
        # move every point to correct position
        pos = (np.matmul(b,np.array(vertices[i])) + offset) * UNIT_CONV[units]
        vertices[i] = tuple(pos) #tuple(UNIT_CONV[units] * ( np.array(vertices[i]) / basis + offset ) )
    for i in range(len(triangles)):
        triangles[i] = tuple(triangles[i])
    
    me = bpy.data.meshes.new("Mesh")
    ob = bpy.data.objects.new("Mesh", me)
    ob.location = bpy.context.scene.cursor.location
    bpy.context.collection.objects.link(ob)
    me.from_pydata(vertices, [], triangles)
    me.update(calc_edges=True)
    
    # set smooth shading
    bm = bmesh.new()
    bm.from_mesh(me)
    for f in bm.faces:
        f.smooth = True
    bm.to_mesh(me)
    bm.free()

    # set material
    mat = "SurfaceRed"
    if not mat in bpy.data.materials:
            _create_new_material(mat, COLORS["red_surface"], alpha)
    
    ob.data.materials.append(bpy.data.materials[mat])

    # make second surface
    b = np.array(basis)
    vertices, triangles = mcubes.marching_cubes(data, -thresh)
    vertices = vertices.tolist()
    triangles = triangles.tolist()
    for i in range(len(vertices)):
        # move every point to correct position
        pos = (np.matmul(b,np.array(vertices[i])) + offset) * UNIT_CONV[units]
        vertices[i] = tuple(pos) #tuple(UNIT_CONV[units] * ( np.array(vertices[i]) / basis + offset ) )
    for i in range(len(triangles)):
        triangles[i] = tuple(triangles[i])
    
    me2 = bpy.data.meshes.new("Mesh")
    ob2 = bpy.data.objects.new("Mesh", me2)
    ob2.location = bpy.context.scene.cursor.location
    bpy.context.collection.objects.link(ob2)
    me2.from_pydata(vertices, [], triangles)
    me2.update(calc_edges=True)
    
    # set smooth shading
    bm = bmesh.new()
    bm.from_mesh(me2)
    for f in bm.faces:
        f.smooth = True
    bm.to_mesh(me2)
    bm.free()

    # set material
    mat = "SurfaceBlue"
    if not mat in bpy.data.materials:
            _create_new_material(mat, COLORS["blue_surface"], alpha)
    
    ob2.data.materials.append(bpy.data.materials[mat])


    ### join both surfaces
    # Deselect all objects in scene.
    for obj in bpy.context.selected_objects:
        obj.select_set(state=False)
    # Select all newly created objects.
    for obj in (ob, ob2):
        obj.select_set(state=True)
    bpy.context.view_layer.objects.active = ob
    bpy.ops.object.join()
    bpy.context.object.name = name
 
    return


def write_voxel_image(fname, cube_data, clip=-1):
    ng = cube_data[1]
    data = cube_data[3]
    print('Creating voxel image at %s' % fname)
    new_shaped_data = np.reshape(data, (int(ng[0]), int(ng[1]*ng[2])))
    new_data = np.zeros( (int(ng[0]), int(ng[1]*ng[2]), 3) )
    if clip < 0:
        amax = np.amax(data)
        amin = np.amin(data)
    else:
        amax = clip
        amin = -clip
    # calculate scaling factor
    scaling = 1. / abs(max(amax,amin))
    new_shaped_data *= scaling
    # make sure maximum values are -1/+1
    new_shaped_data[new_shaped_data < -1.] = -1.
    new_shaped_data[new_shaped_data > 1.] = 1.
    print(amax, amin, scaling)
    # put positive values to first channel and negative values to second channel
    new_data[:,:,0] = np.where(new_shaped_data > 0., new_shaped_data, 0.)
    new_data[:,:,1] = np.where(new_shaped_data < 0., -new_shaped_data, 0.)
    #new_data[:,:,0] = new_shaped_data[:,:]
    #new_data = np.abs(new_data)
    # renormalize because PIL expects uint8 data from 0 to 255
    #new_data = np.uint8((new_data - np.amin(new_data)) / (np.amax(new_data) - np.amin(new_data)) * 255)
    new_data = np.uint8(new_data*255)
    print(new_data)
    print(new_data.shape)
    print(np.amax(new_data))
    print(np.amin(new_data))
    data_image = pimg.fromarray(new_data)
    #data_image = data_image.convert('RGB')
    print(np.amax(data_image))
    data_image.save(fname)

 
    

class Atom:
    """A single atom.

    Attributes:
        at_num (int): The atomic number of the atom.
        location (numpy array): The xyz location of the atom, in 
            Angstroms.
        id_num (int): A unique identifier number.
    """

    def __init__(self, atomic_number, location, id_num):
        self.at_num = atomic_number
        self.location = location # np.array
        self.id_num = id_num

    def draw(self, color='by_element', radius=None, units='nm', 
             scale=1.0, subsurf_level=2, segments=16):
        """Draw the atom in Blender.

        Args:
            color (string, ='by_element'): If None, coloring is done by
                element. Otherwise specifies the color.
            radius (string, =None): If None, draws at the van der Waals
                radius. Otherwise specifies the radius in angstroms.
            units (sting, ='nm'): 1 BU = 1 nm by default. Can also be
                set to angstroms.
            scale (float, =1.0): Scaling factor for the atom. Useful
                when generating ball-and-stick models.
            subsurf_level (int, =2): Subsurface subdivisions that will
            	be applied.
            segments (int, =16): Number of segments in each UV sphere
                primitive

        Returns:
            The blender object.
        """

        # The corrected location (i.e., scaled to units.)
        loc_corr = tuple(c*UNIT_CONV[units] for c in self.location)

        # Work out the sphere radius in BU.
        if not radius:
            rad_adj = RADII[self.at_num]*UNIT_CONV[units]*scale
        else:
            rad_adj = radius*UNIT_CONV[units]*scale

        # Create sphere as bmesh.
        bm = bmesh.new()
        bmesh.ops.create_uvsphere(bm, 
                                  u_segments=segments, 
                                  v_segments=segments,
                                  diameter=rad_adj)
        
        for f in bm.faces:
            f.smooth = True
        
        # Convert to mesh.
        me = bpy.data.meshes.new("Mesh")
        bm.to_mesh(me)
        bm.free()

        # Assign mesh to object and place in space.
        atom_sphere = bpy.data.objects.new("atom({})_{}".format(
                            self.at_num, self.id_num), me)
        bpy.context.collection.objects.link(atom_sphere)
        
        atom_sphere.location = loc_corr

        # Assign subsurface modifier, if requested
        if subsurf_level != 0:
            atom_sphere.modifiers.new('Subsurf', 'SUBSURF')
            atom_sphere.modifiers['Subsurf'].levels = subsurf_level

        # Color atom and assign material
        if color == 'by_element':
            atom_color = ELEMENT_COLORS[self.at_num]
        else:
            atom_color = color

        if not atom_color in bpy.data.materials:
            _create_new_material(atom_color, COLORS[atom_color])

        atom_sphere.data.materials.append(bpy.data.materials[atom_color])

        return atom_sphere


class Bond:
    """A bond between two atoms.

    Attributes:
        atom1 (atom): The first atom in the bond.
        atom2 (atom): The second atom in the bond.
    """

    def __init__(self, atom1, atom2):
        self.atom1 = atom1
        self.atom2 = atom2

    @staticmethod
    def _draw_half(location, length, rot_angle, rot_axis, element, 
                   radius=0.2, color='by_element', units='nm',
                   vertices=64, edge_split=False, start=0, end=0):
        """Draw half of a bond (static method).

        Draws half of a bond, given the location and length. Bonds are
        drawn in halves to facilitate coloring by element.

        Args:
            location (np.array): The center point of the half bond.
            length (float): The length of the half bond.
            rot_angle (float): Angle by which bond will be rotated.
            rot_axis (np.array): Axis of rotation.
            element (int): atomic number of element of the bond (for 
                coloring).
            radius (float, =0.2): radius of the bond.
            color (string, ='by_element'): color of the bond. If
                'by_element', uses element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The new bond (Blender object).
        """

        loc_corr = tuple(c*UNIT_CONV[units] for c in location)
        len_corr = length * UNIT_CONV[units]
        radius_corr = radius * UNIT_CONV[units]
        start = start * UNIT_CONV[units]
        end = end * UNIT_CONV[units]
        if False:
            bpy.ops.mesh.primitive_cylinder_add(vertices=vertices,
                                            radius=radius_corr, 
                                            depth=len_corr, location=loc_corr, 
                                            end_fill_type='NOTHING')
            print(rot_axis)
            rot_matrix = create_z_orient(rot_axis)
            bpy.ops.transform.rotate(value=rot_angle, orient_axis='Z', orient_matrix=rot_matrix)
        else:
            dx = end[0] - start[0]
            dy = end[1] - start[1]
            dz = end[2] - start[2]
            dist = math.sqrt(dx**2 + dy**2 + dz**2)

            bpy.ops.mesh.primitive_cylinder_add(vertices=vertices,
                                            radius=radius_corr, 
                                            depth=len_corr, location=(dx/2+start[0], dy/2+start[1], dz/2+start[2]), 
                                            end_fill_type='NOTHING')            

            phi = math.atan2(dy, dx) 
            theta = math.acos(dz/dist) 

            bpy.context.object.rotation_euler[1] = theta 
            bpy.context.object.rotation_euler[2] = phi 
        
        bpy.ops.object.shade_smooth()

        if edge_split:
            bpy.ops.object.modifier_add(type='EDGE_SPLIT')
            bpy.ops.object.modifier_apply(modifier='EdgeSplit')

        if color == 'by_element':
            bond_color = ELEMENT_COLORS[element]
        else:
            bond_color = color

        if not bond_color in bpy.data.materials:
            _create_new_material(bond_color, COLORS[bond_color])

        bpy.context.object.data.materials.append(
            bpy.data.materials[bond_color])

        return bpy.context.object


    def draw(self, radius=0.2, color='by_element', units='nm',
             vertices=64, edge_split=False):
        """Draw the bond as two half bonds (to allow coloring).

        Args:
            radius (float, =0.2): Radius of cylinder in angstroms.
            color (string, ='by_element'): Color of the bond. If
                'by_element', each half gets element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The bond (Blender object), with both halves joined.
        """

        created_objects = []

        center_loc = (self.atom1.location + self.atom2.location)/2
        bond_vector = self.atom1.location - self.atom2.location
        length = np.linalg.norm(bond_vector)

        bond_axis = bond_vector/length
        cyl_axis = np.array((0,0,1))
        rot_axis = np.cross(bond_axis, cyl_axis)
        angle = -np.arccos(np.dot(cyl_axis, bond_axis))

        start_center = (self.atom1.location + center_loc)/2
        created_objects.append(Bond._draw_half(start_center, length/2, angle, 
                               rot_axis, self.atom1.at_num, radius, color, 
                               units, vertices, edge_split, self.atom1.location, center_loc))

        end_center = (self.atom2.location + center_loc)/2
        created_objects.append(Bond._draw_half(end_center, length/2, angle, 
                               rot_axis, self.atom2.at_num, radius, color, 
                               units, vertices, edge_split, center_loc, self.atom2.location))

        for obj in bpy.context.selected_objects:
            obj.select_set(state=False)

        for obj in created_objects:
            obj.select_set(state=True)

        bpy.ops.object.join()
        bpy.context.object.name = "bond_{}({})_{}({})".format(
            self.atom1.id_num, self.atom1.at_num, self.atom2.id_num, 
            self.atom2.at_num)
        
        return bpy.context.object


class Molecule:
    """The molecule object.

    Attributes:
        atoms (list, = []): List of atoms (atom objects) in molecule.
        bonds (list, = []): List of bonds (bond objects) in molecule.
    """

    def __init__(self, name='molecule', atoms=None, bonds=None):
        self.name = name
        if atoms == None:
            self.atoms = []
        else:
            self.atoms = atoms
        if bonds == None:
            self.bonds = []
        else:
            self.bonds = bonds
        self.cubes = []
    
    def add_atom(self, atom):
        """Adds an atom to the molecule."""
        self.atoms.append(atom)

    def add_bond(self, a1id, a2id):
        """Adds a bond to the molecule, using atom ids."""
        if not self.search_bondids(a1id, a2id):
            self.bonds.append(Bond(self.search_atomid(a1id), 
                                   self.search_atomid(a2id)))

    def search_atomid(self, id_to_search):
        """Searches through atom list and returns atom object
        corresponding to (unique) id."""
        for atom in self.atoms:
            if atom.id_num == id_to_search:
                return atom
        return None
    
    def guess_bonds(self):
        """Calculate distance matrix between all atom coordinates
        and guess bonds between atoms."""
        coords = np.array( [ self.atoms[i].location for i in range(len(self.atoms)) ] )
        ids = [ self.atoms[i].id_num for i in range(len(self.atoms)) ]
        print(coords)
        dists = distance_matrix(coords, coords)
        print(dists)
        for x in range(1,len(dists)):
            for y in range(0, x):
                if dists[x][y] < 3.2 and x != y:
                    # add bond
                    self.add_bond(ids[x], ids[y])
        print(self.bonds)

    def search_bondids(self, id1, id2):
        """Searches through bond list and returns bond object
        corresponding to (unique) ids."""
        for b in self.bonds:
            if ((id1, id2) == (b.atom1.id_num, b.atom2.id_num) or
                    (id2, id1) == (b.atom1.id_num, b.atom2.id_num)):
                return b
        return None

    def draw_bonds(self, caps=True, radius=0.2, color='by_element', 
                   units='nm', join=True, with_H=True, subsurf_level=1,
                   vertices=64, edge_split=False):
        """Draws the molecule's bonds.

        Args:
            caps (bool, =True): If true, each bond capped with sphere of
                radius at atom position. Make false if drawing
                ball-and-stick model using separate atom drawings.
            radius (float, =0.2): Radius of bonds in angstroms.
            color (string, ='by_element'): Color of the bonds. If
                'by_element', each gets element coloring.
            units (string, ='nm'): 1 BU = 1 nm, by default. Can change
                to angstroms ('A').
            join (bool, =True): If true, all bonds are joined together
                into a single Bl object.
            with_H (bool, =True): Include H's.
            subsurf_level (int, =1): Subsurface subdivisions that will
            	be applied to the atoms (end caps).
            vertices (int, =64): Number of vertices in each bond
                cylinder.
            edge_split (bool, =False): Whether to apply the edge split
                modifier to each bond.

        Returns:
            The bonds as a single Blender object, if join=True.
            Otherwise, None.
        """
        
        created_objects = []

        for b in self.bonds:
            if with_H or ( b.atom1.at_num != 1 and b.atom2.at_num != 1 ):
                created_objects.append(b.draw(radius=radius,
                                              color=color, 
                                              units=units, 
                                              vertices=vertices,
                                              edge_split=edge_split))

        if caps:
            for a in self.atoms:
                if with_H or a.at_num != 1:
                    created_objects.append(a.draw(color=color, 
                                                  radius=radius, 
                                                  units=units,
                                                  subsurf_level=subsurf_level))
        
        if join:
            # Deselect anything currently selected.
            for obj in bpy.context.selected_objects:
                obj.select_set(state=False)

            # Select drawn bonds.
            for obj in created_objects:
                obj.select_set(state=True)

            if len(created_objects) > 0:
                bpy.ops.object.join()


            bpy.context.object.name = self.name + '_bonds'
            
            return bpy.context.object

        else:
            return None


    def draw_atoms(self, color='by_element', radius=None, units='nm', 
                   scale=1.0, join=True, with_H=True, subsurf_level=2,
                   segments=16):
        """Draw spheres for all atoms.

        Args: 
            color (str, ='by_element'): If 'by_element', uses colors in
                ELEMENT_COLORS. Otherwise, can specify color for whole
                model. Must be defined in COLORS.
            radius (float, =None): If specified, gives radius of all 
                atoms.
            units (str, ='nm'): Units for 1 BU. Can also be A.
            join (bool, =True): If true, all atoms are joined together
                into a single Bl object.
            with_H (bool, =True): Include the hydrogens.
            subsurf_level (int, =2): Subsurface subdivisions that will
            	be applied to the atoms.
            segments (int, =16): Number of segments in each UV sphere
                primitive

        Returns:
            The atoms as a single Blender object, if join=True.
            Otherwise, None.
        """

        # Store start time to time script.
        start_time = time.time()

        # Holds links to all created objects, so that they can be
        # joined.
        created_objects = []

        # Initiate progress monitor over mouse cursor.
        bpy.context.window_manager.progress_begin(0, len(self.atoms))
        
        n = 0
        for a in self.atoms:
            if with_H or a.at_num != 1:
                created_objects.append(a.draw(color=color, radius=radius, 
                                              units=units, scale=scale,
                                              subsurf_level=subsurf_level,
                                              segments=segments))
            n += 1
            bpy.context.window_manager.progress_update(n)

        # End progress monitor.
        bpy.context.window_manager.progress_end()

        if join:
            # Deselect all objects in scene.
            for obj in bpy.context.selected_objects:
                obj.select_set(state=False)
            # Select all newly created objects.
            for obj in created_objects:
                obj.select_set(state=True)
            bpy.context.view_layer.objects.active = created_objects[0]
            bpy.ops.object.join()
            bpy.context.object.name = self.name + '_atoms'
            
        print("{} seconds".format(time.time()-start_time))
        return




    def read_cube(self, filename):
        """Loads octopus cube file
        
        Args:
            filename (string): The target file.
        """
        
        print("Reading file", filename)
        with open(filename) as f:
            txt = f.readlines()

        offset = np.array( [ float(txt[2].split()[1]), float(txt[2].split()[2]), float(txt[2].split()[3]) ] )
        print("Grid offset: ", offset)
        ng = [ abs(int(txt[3].split()[0])), abs(int(txt[4].split()[0])), abs(int(txt[5].split()[0])) ]
        print("Grid size: ", ng[0], ng[1], ng[2])

        basis = [ np.array( [float(txt[3+abc].split()[1+xyz]) for xyz in range(3)] ) for abc in range(3) ]
        print("Basis vectors for grid are:")
        for abc in range(3):
            print(basis[abc])

        # read molecule data
        idnum = 1
        for i, line in enumerate(txt[6:]):
            line = line.split()
            if len(line) == 6:
                break # beginning of volumetric data
            atnum = int(line[0])
            coords = np.array( [ float(line[2+xyz]) for xyz in range(3) ] )
            self.add_atom(Atom(atnum, coords, idnum))
            idnum += 1

        natoms = idnum - 1
        # read volumetric data
        data =[] # np.zeros(ng)
        for line in txt[6+natoms:]:
            line = line.split()
            for element in line:
                data.append(float(element))
        data = np.array( data )
        data.shape = tuple(ng)

        self.cubes.append( [offset, ng, basis, data] )

        return  # offset, ng, basis, data

    def read_pdb(self, filename):
        """Loads a pdb file into a molecule object. Only accepts atoms
        with Cartesian coords through the ATOM/HETATM label and bonds
        through the CONECT label.

        Args:
            filename (string): The target file.
        """

        with open(filename) as pdbfile:
            for line in pdbfile:
                if line[0:4] == "ATOM":
                    idnum = int(line[6:11])
                    atnum = ATOMIC_NUMBERS[line[76:78].strip().upper()]
                    coords = np.array((float(line[30:38]), float(line[38:46]), 
                                       float(line[46:54])))
                    self.add_atom(Atom(atnum, coords, idnum))

                elif line[0:6] == "HETATM":
                    idnum = int(line[6:11])
                    atnum = ATOMIC_NUMBERS[line[76:78].strip().upper()]
                    coords = np.array((float(line[30:38]), float(line[38:46]), 
                                       float(line[46:54])))
                    self.add_atom(Atom(atnum, coords, idnum))

                elif line[0:6] == "CONECT":

                    # Loads atoms as a list. First atom is bonded to the
                    # remaining atoms (up to four).
                    atoms = line[6:].split()
                    for bonded_atom in atoms[1:]:
                        # print(atoms[0], bonded_atom)
                        self.add_bond(int(atoms[0]), int(bonded_atom))


if __name__ == '__main__':
    cwd = os.getcwd()
    dirs = os.listdir(cwd)
    dirs.sort()

    counter = 0
    for directory in dirs:
        if directory.find('td.') == -1:
            continue
        cubepath = "%s/density.cube" % directory
        imagepath = 'density_%05i.png' % counter
        print("Reading %s and writing %s" % (cubepath, imagepath))
        m = Molecule()
        m.read_cube("%s/density.cube" % directory)
        write_voxel_image('density_%05i.png' % counter, m.cubes[0], 1e-4)
        counter += 1
    
